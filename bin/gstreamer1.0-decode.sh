#!/bin/sh

set -e

fail_exit(){
	echo "FAILED: $0"
	exit 1
}
trap fail_exit EXIT

. common/update-test-path

DATAPATH=${TESTPATH}/${1}
VIDEOS_DIR=/home/shared/Videos

MEDIA=\
"Audio_Wikinews_Brief_2008-02-27-speex.ogg    audio
 big_buck_bunny_480p.webm                     video
 Example.ogg                                  audio
 Maqam_Rast.flac.ogg                          audio
 trailer_400p.ogg                             video"

echo "$MEDIA" | while read FILE TYPE; do
    FILEPATH=${DATAPATH}/$FILE
    if [ ! -e "$FILEPATH" ]; then
      FILEPATH=$VIDEOS_DIR/$FILE
    fi
    PLAYBIN="playbin uri=\"file://$FILEPATH\""
    AUDIOSINK="audio-sink=fakesink"
    VIDEOSINK=""
    if [ "$TYPE" = "video" ] ; then
        VIDEOSINK="video-sink=fakesink"
    fi

    echo "Playing $FILEPATH"
    gst-launch-1.0 $PLAYBIN $AUDIOSINK $VIDEOSINK
done

trap '' EXIT
echo "PASSED: $0"
