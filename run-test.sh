#!/bin/sh

# Copyright: 2012-2018 Collabora ltd.
# Author: Cosimo Alfarano <cosimo.alfarano@collabora.co.uk>
# Author: Andre Moreira Magalhaes <andre.magalhaes@collabora.co.uk>

# Run all tests, mainly to be user under Lava, with a custom parser

# a list of .sh which are not actually a test, so we can silently ignore
NOT_TESTS=""
# test that we need to skip, and let LAVA know about it
SKIP_TESTS=""

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
. "${TESTDIR}/config.sh"

. "${TESTDIR}/common/update-test-path"

PREFIX="${TESTDIR}/gstreamer1.0"

num_failed=0

export RESULTDIR=""
export DEBUG=1

cleanup() {
  rm -f "$TMP"
}

TMP=$(mktemp)
for TEST in ${PREFIX}/*.sh; do
  unset skip
  for AVOID_TEST in ${SKIP_TESTS}; do
    if [ "$(basename ${TEST} .sh)" = "$(basename ${AVOID_TEST} .sh)" ]; then
      echo ${TEST}: SKIP -
      skip=true
    fi
  done
  for AVOID_TEST in ${NOT_TESTS}; do
    if [ "$(basename ${TEST} .sh)" = "$(basename ${AVOID_TEST} .sh)" ]; then
      # we don't need to notify LAVA about this skip.
      skip=true
    fi
  done
  test -n "$skip" && continue

  echo undecided > "$TMP"
  {
    set +e
    GST_DEBUG_NO_COLOR=1 GST_DEBUG='*:4' sh ${TEST} ${TESTDIR}
    echo $? > "$TMP"
  } 2>&1 | sed -e 's/^/# /'
  OUTCOME="$(cat "$TMP")"

  if [ "$OUTCOME" != 0 ]; then
        echo ${TEST}: FAILED - $RESULTDIR
        num_failed=$(($num_failed + 1))
  else
        echo ${TEST}: PASSED - $RESULTDIR
  fi
done
cleanup
exit $num_failed
