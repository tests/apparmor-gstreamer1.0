debug()
{
   test -n $DEBUG && echo $*
}

fatalerror()
{
   echo Fatal Error: $1
   _error="fatal error"
   exit 127
}

test_exit()
{
    cleanup
    if [ -z "$_error" ]; then
        echo $TEST: PASSED
    else
        echo $TEST: FAILED - $_error
    fi
}

error_handler() {
    _error="generic error handler"
    test_exit
    exit 127
}

cleanup()
{
    for ACTION in ${CLEANUP_ACTIONS}; do
        echo $TEST: cleanup: $ACTION
        FILE="$(echo $ACTION|sed -ne "s/[^:]*:\(.*\)/\1/p")"
        case "$ACTION" in
            profile:*)
                sudo apparmor_parser -R $FILE
                ;;
            file:*)
                rm -f $FILE
                ;;
            *)
                echo SELECTOR $SELECTOR unknown
                _error="bad selector $SELECTOR"
                exit 127
        ;;
    esac
    done
}

initialize()
{
    debug TEST=$1 V=$2 "($3)"
    export TEST="$1"
    export TEST_CASE_VERSION=$2
    export TEST_CASE_URL=$3
    debug Creating cleanup actions array
    CLEANUP_ACTIONS=

    trap "error_handler" EXIT
    set -e
}

shutdown()
{
    trap - EXIT
    set -e
    test_exit
}

get_tmpfile() {
    mktemp --tmpdir $(echo $TEST | sed 's,/,_,g').XXXXXXXXXX
}

fail() {
    echo TEST $1: FAIL - $2
}

assert_file_empty()
{
    FILE=$1
    if [ -s "$FILE" ]; then
        echo $TEST: assertion failed - $FILE not empty
    echo "|---- Contents of $FILE:"
    cat "$FILE"
    echo "|----"
    _error="assertion failed"
    exit 127
    fi
    return 0
}

assert_file_match() {
    FILE=$1
    EXPR=$2
    if grep -q "${EXPR}" $FILE; then
        return 0
    fi
    echo $TEST: assertion failed - $EXPR not present in $FILE
    _error="assertion failed"
    exit 127
}

extract_aa_tokens() {
    LOG=$1
    TOKENS=$2 # e.g. PERMITTING
    OUTFILE=$3
    debug "|---- Raw audit log:"
    if test -n "$DEBUG"; then sudo cat ${LOG}; fi
    debug "|----"
    apparmor_parse_journal ${LOG} ${TOKENS} > "${OUTFILE}"
    debug "|---- Summarized:"
    if test -n "$DEBUG"; then cat ${OUTFILE}; fi
    debug "|----"
}

make_profile() {
    BINARY=$1
    shift
    RULES=$*

    OUTFILE=`get_tmpfile`

    ${PREFIX}/mkprofile.pl $BINARY \
        abstraction:chaiwala-base \
    $RULES > $OUTFILE
    debug "|---- Loading AppArmor profile for $BINARY with $RULES:"
    if test -n "$DEBUG"; then cat ${OUTFILE}; fi
    debug "|----"

    sudo apparmor_parser $OUTFILE
    CLEANUP_ACTIONS="$CLEANUP_ACTIONS profile:$OUTFILE"
    CLEANUP_ACTIONS="$CLEANUP_ACTIONS file:$OUTFILE"
}
