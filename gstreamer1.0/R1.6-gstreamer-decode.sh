#!/bin/sh

TESTDIR=${1}
. "${TESTDIR}/config.sh"
. "${TESTDIR}/common/common-apparmor.sh"
. "${TESTDIR}/common/update-test-path"

PREFIX="${TESTDIR}/gstreamer1.0"

CURSOR="$(journalctl --show-cursor -t audit -n 0 -o cat)"
CURSOR="${CURSOR#-- cursor: }"
AA_DUMP="aa-dump_$(date +%Y%m%d-%H%M%S)"
AA_TMPDIR="$(mktemp -d /tmp/apparmor-complaints.XXXXXX)"
TMPDIR="${AA_TMPDIR}/${AA_DUMP}"
mkdir "${TMPDIR}"

. "${PREFIX}/general.inc"

initialize "AppArmor:R.16/gstreamer1.0-decode" 0.5 \
	"https://wiki.apertis.org/QA/Test_Cases/apparmor-gstreamer1.0"

make_profile ${TESTDIR}/gstreamer1.0/gstreamer1.0-decode.sh \
	abstraction:chaiwala-base abstraction:gstreamer-1.0 \
	abstraction:python /proc/*/mounts:r /usr/bin/python*:rix \
	capability:dac_override ${TESTDIR}/chaiwala-test-media/**:rixm \
	$pwd/:r

${TESTDIR}/bin/gstreamer1.0-decode.sh chaiwala-test-media

AUDIT_FILE="${TMPDIR}/audit.log"
RESULT="${TMPDIR}/complaint_tokens.log"

journalctl --after-cursor "${CURSOR}" -t audit -o cat > ${AUDIT_FILE}
extract_aa_tokens ${AUDIT_FILE} "DENIED" $RESULT
assert_file_empty $RESULT
shutdown
